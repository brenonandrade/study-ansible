# O que é?

![AnsibleLogo](./pics/awx.png)

é um projeto comunitário de código aberto, patrocinado pela Red Hat, que permite aos usuários controlar melhor o uso do projeto Ansible pela comunidade em ambientes de TI. AWX é o projeto upstream do qual o componente do controlador de automação é derivado.

- Desenvolvido em python
- Facil de instalar e configurar
- Escalável
- Suporta a maioria dos sistemas operacionais, clouds, networks
- Como usa o ansible não precisa de agente, somente utilizar protocolos nativos ssh e winrm
- Idempotente, uma tarefa não executa varias vezes
- Controle de versão
- Integracao com ITSM
- Integracao com cofres de senhas
  - As pessoas não precisam ter acesso a senhas
- Interface Web e Restful APi, podendo chamar automações pela api
- Infinidade de módulos e plugins
- Aprovações de automações
- RBAC de usuãrio para dar permissões para usuarios e grupos
- Segregação de times
- Logs centralizados
- Agendador de tarefas e notificações
- Orquestrações de automações
- Controle e Execução desacomplados

## O projeto

Faz parte do Cloud Native Landscape e a comunidade é bastante ativa

<https://aws.amazon.com/marketplace/pp/prodview-csuubwvckv24c>

<https://www.youtube.com/watch?v=lCs_RBCeSZA&t=628s>

<https://github.com/ansible/awx> \
<https://www.ansible.com/products/awx-project/faq>

lista de discussoes \
<https://groups.google.com/g/awx-project>

![AnsibleSimpleArchiteture](./pics/ansible-architecture.png)

![AnsibleSimpleArchiteture](./pics/simplearch.jpg)

### **Componentes**

- Controle e Execução desacomplados
- Ansible
- Nginx
- Redis
- PostegreSQL
- AnsibleRunner
- Receptor
- Django
