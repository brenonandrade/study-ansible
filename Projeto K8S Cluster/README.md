# K8s Cluster Na AWS Usando Ansible

Esse projeto consiste em utilizar somente o Ansible para criar um cluster Kubernetes na AWS e utilizar durante o projeto recusos que são essenciais saber para quem trabalha diretamente com Ansible. Apesar do Terraform na minha opinião ser a melhor ferramenta para provisionamento de cloud também podemos fazer isso via Ansible. Claro que poderíamos somar o melhor das duas ferramentas aliado ainda ao Packer, mas vamos nos manter somente com o ansible para ganhar mais conhecimento.

Para esse projeto vamos aprender como criar uma boa estrutura de diretórios para os seus playbooks e como separar os arquivos afim de ficar bem prático de de gerênciar e dar manutenção. Para isso o ansible fornece um comando chamado [ansible-galaxy](https://galaxy.ansible.com/home) que ajuda a criar uma estrutura de diretório muito boa. Além disso o ansible-galaxy é utilizado para instalar collections de modules desenvolvido pela comunidade.

## Requisitos

- Uma conta na [WS](https://portal.aws.amazon.com/billing/signup?refid=c623d581-46f6-43a2-b227-cabbee9cd673&redirect_url=https%3A%2F%2Faws.amazon.com%2Fregistration-confirmation&language=pt_br#/start/email)

- Ansible instalado. Veja o [tutorial de instalação](../README.md) caso necessário.

## Fases do projeto

- 1 Provisionamento => Esta fazer precisamos montar a infra estrutura na AWS
- 2 Setup => Precisamos instalar e configurar o cluster
- 3 Deploy => Vamos deployar uma aplicaçao exemplo no cluster
- 4 Monitoramento => Vamos instalar o Prometheus e o Grafana para monitorar o nosso cluster

## Estrutura de diretórios

Dentro da pasta [src](./src/) será de fato onde o projeto acontecerá.
Esta diretório conterá o diretório das fases

Dentro de cada fase teremos os seguintes arquivos.

- o hosts o nosso inventário para cada fase
- O ansible.cfg para ter as configurações do nosso ansible para cada fase.
- O diretório roles que de fato terá os nossos playbooks
- Um README.md explicando cada fase do projeto.

```bash
~/projects/ansible/study-ansible/Projeto K8S Cluster main !5 ?2                                                                                                                    1.1.7 11:51:00
❯ touch ansible.cfg hosts main.yml 
```

Vamos criar uma pasta chamada roles que dentro dela terão os nossos playbooks, nossas tasks. Para isso vamos utilizar o binário chamado ansible-galaxy. Junto com o ansible vem vários binários que nos ajudam além do galaxy, vale a pena conferir.

![Binarios Ansible](./pics/binarios%20ansibles.jpg)

Vamos montar os diretórios referentes as fases.

```bash
~/projects/ansible/study-ansible/Projeto K8S Cluster main !5 ?2                                                                                                                    1.1.7 11:41:43
❯ cd roles 

~/projects/ansible/study-ansible/Projeto K8S Cluster/roles main !5 ?2                                                                                                              1.1.7 11:41:48
❯ ansible-galaxy init provisioning     
- Role provisioning was created successfully

~/projects/ansible/study-ansible/Projeto K8S Cluster/roles main !5 ?2                                                                                                              1.1.7 11:41:59
❯ ansible-galaxy init setup       
- Role setup was created successfully

~/projects/ansible/study-ansible/Projeto K8S Cluster/roles main !5 ?2                                                                                                              1.1.7 11:42:05
❯ ansible-galaxy init deploy_app       
- Role deploy_app was created successfully

~/projects/ansible/study-ansible/Projeto K8S Cluster/roles main !5 ?2                                                                                                              1.1.7 11:42:21
❯ ansible-galaxy init deploy_monitoring
- Role deploy_monitoring was created successfully

~/projects/ansible/study-ansible/Projeto K8S Cluster/roles main !5 ?2                                                                                                              1.1.7 11:42:28
```

Dentro de cada uma das nossas tasks podem existir sub-tasks, ou até mesmo sub-sub-tasks. Logo será necessário criar uma pasta roles dentro de cada uma das nossas roles iniciais, mas vamos fazendo isso ao longo do caminho de acordo o necessário.

```bash
~/projects/ansible/study-ansible/Projeto K8S Cluster main !5 ?2                                                                                                                    1.1.7 11:51:11
❯ tree   
.
├── ansible.cfg
├── hosts
├── main.yml
├── pics
│   └── binarios ansibles.jpg
├── README.md
└── roles
    ├── deploy_app
    │   ├── defaults
    │   │   └── main.yml
    │   ├── files
    │   ├── handlers
    │   │   └── main.yml
    │   ├── meta
    │   │   └── main.yml
    │   ├── README.md
    │   ├── tasks
    │   │   └── main.yml
    │   ├── templates
    │   ├── tests
    │   │   ├── inventory
    │   │   └── test.yml
    │   └── vars
    │       └── main.yml
    ├── deploy_monitoring
    │   ├── defaults
    │   │   └── main.yml
    │   ├── files
    │   ├── handlers
    │   │   └── main.yml
    │   ├── meta
    │   │   └── main.yml
    │   ├── README.md
    │   ├── tasks
    │   │   └── main.yml
    │   ├── templates
    │   ├── tests
    │   │   ├── inventory
    │   │   └── test.yml
    │   └── vars
    │       └── main.yml
    ├── provisioning
    │   ├── defaults
    │   │   └── main.yml
    │   ├── files
    │   ├── handlers
    │   │   └── main.yml
    │   ├── meta
    │   │   └── main.yml
    │   ├── README.md
    │   ├── tasks
    │   │   └── main.yml
    │   ├── templates
    │   ├── tests
    │   │   ├── inventory
    │   │   └── test.yml
    │   └── vars
    │       └── main.yml
    └── setup
        ├── defaults
        │   └── main.yml
        ├── files
        ├── handlers
        │   └── main.yml
        ├── meta
        │   └── main.yml
        ├── README.md
        ├── tasks
        │   └── main.yml
        ├── templates
        ├── tests
        │   ├── inventory
        │   └── test.yml
        └── vars
            └── main.yml

38 directories, 37 files
```

## Hosts - detalhes

Sobre hosts vale a pena aprender algumas coisas
<https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html>
Se ver a documentação vai observar que é possível passar variávieis para todos os hosts ou hosts específicos.

Definir qual o caminho do interpretador python, isso evita que ele saia procurando e caso não esteja no path dar algum tipo de erro
para isso definimos o **ansible_python_interpreter**

vamos começar com um hosts assim:

```yaml
#variavies globaix
[all:vars]
ansible_python_interpreter=/usr/bin/python
ntp_server=br.pool.ntp.org

[local]
localhost 

#variaveis do grupo local
[local:vars]
ansible_connection=local
gather_facts=false

# será preenchido depois automaticamente 
[kubernetes]
```

## Main.yml - detalhes

Este somente servirá para fazer includes (chamar outro arquivo) das nossas fases de acordo com o que vamos fazendo, então será preenchido de acordo com o nosso desenvolvimento, mas vamos deixar o primeiro chamado como exemplo.

```yaml
---
# Somente includes aqui das tasks
- include: provisioning
```

## Aprendizados durante o projeto

O uso de tags é interessante pois voce pode executar somente um playbook específico utilizando, ou até mesmo negar alguns.

somente executará as tagsA e tagB

```bash
ansible-playbook -i hosts main.yml --tags "tagA,tagB" 
```

não vai executar a tagA e tagB

```bash
ansible-playbook -i hosts main.yml --skip-tags "tagA,tagB"
```
