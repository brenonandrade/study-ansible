- name: Procurando AMI Ubuntu-minimal latest
  shell: aws ec2 describe-images --region {{ region }} --owners 099720109477 --output text --filters "Name=name,Values=ubuntu-minimal/images/hvm-ssd/ubuntu-*{{ ubuntu_version }}-{{ arquitetura }}*" --query 'sort_by(Images, &CreationDate)[-1].[ImageId]' --output text|  tail -n1
  changed_when: False
  register: ubuntu_ami_id

- name: Criando o security group
  amazon.aws.ec2_group:
    name: "{{ sec_group_name }}"
    description: sg for kubernetes
    profile: "{{ profile }}"
    region: "{{ region }}"
    rules:
    - proto: tcp
      from_port: 22
      to_port: 22
      cidr_ip: 0.0.0.0/0
      rule_desc: SSH
    - proto: tcp
      from_port: 2379
      to_port: 2380
      cidr_ip: 0.0.0.0/0
      rule_desc: etcd server API
    - proto: tcp
      from_port: 6443
      to_port: 6443
      cidr_ip: 0.0.0.0/0
      rule_desc: kube-apiserver
    - proto: tcp
      from_port: 10250
      to_port: 10250
      cidr_ip: 0.0.0.0/0
      rule_desc: Kubelet API
    - proto: tcp
      from_port: 10257
      to_port: 10257
      cidr_ip: 0.0.0.0/0
      rule_desc: kube-controller-manager
    - proto: tcp
      from_port: 10259
      to_port: 10259
      cidr_ip: 0.0.0.0/0
      rule_desc: kube-scheduler
    - proto: tcp
      from_port: 30000
      to_port: 32767
      cidr_ip: 0.0.0.0/0
      rule_desc: NodePort Services
    - proto: tcp
      from_port: 6783
      to_port: 6783
      cidr_ip: 0.0.0.0/0
      rule_desc: WeaveNet
    - proto: udp
      from_port: 6783
      to_port: 6783
      cidr_ip: 0.0.0.0/0
      rule_desc: Weavenet
    - proto: udp
      from_port: 6784
      to_port: 6784
      cidr_ip: 0.0.0.0/0
      rule_desc: Weavenet
    rules_egress:
    - proto: all
      cidr_ip: 0.0.0.0/0
    tags:
      name: k8s sec group
  register: basic_firewall

- name: Criando as instancias ec2 masters
  # https://docs.ansible.com/ansible/latest/collections/amazon/aws/ec2_module.html
  amazon.aws.ec2_instance:
    profile: "{{ profile }}"
    region: "{{ region }}"
    instance_type: "{{ instance_type_masters }}"
    image_id: "{{ ubuntu_ami_id.stdout }}"
    key_name: "{{ key_name }}"
    security_group: "{{ sec_group_name }}"
    exact_count : "{{ number_instances_masters }}"
    name: k8s-node-master
    state: running
    wait: true
  register: ec2_instances_masters

- name: Criando as instancias ec2 workers
  amazon.aws.ec2_instance:
    profile: "{{ profile }}"
    region: "{{ region }}"
    instance_type: "{{ instance_type_workers }}"
    image_id: "{{ ubuntu_ami_id.stdout }}"
    key_name: "{{ key_name }}"
    security_group: "{{ sec_group_name }}"
    exact_count : "{{ number_instances_workers }}"
    name: k8s-node-workers
    state: running
    wait: true
  register: ec2_instances_workers

- name: Criando as instancias ec2 etcds
  amazon.aws.ec2_instance:
    profile: "{{ profile }}"
    region: "{{ region }}"
    instance_type: "{{ instance_type_etcds }}"
    image_id: "{{ ubuntu_ami_id.stdout }}"
    key_name: "{{ key_name }}"
    security_group: "{{ sec_group_name }}"
    exact_count : "{{ number_instances_etcds }}"
    name: k8s-node-etcds
    state: running
    wait: true
  register: ec2_instances_etcds

- name: "Tag instance(s) masters"
  amazon.aws.ec2_tag:
    region: "{{ region }}"
    resource: "{{ item.instance_id }}"
    profile: "{{ profile }}"
    state: present
    tags:
      Name: k8s-node-master-{{ item.ami_launch_index|int + 1 }}
      CreatedBy: ansible
      Environment: study
  with_items: "{{ ec2_instances_masters.instances }}"

- name: "Tag instance(s) workers"
  amazon.aws.ec2_tag:
    region: "{{ region }}"
    resource: "{{ item.instance_id }}"
    profile: "{{ profile }}"
    state: present
    tags:
      Name: k8s-node-worker-{{ item.ami_launch_index|int + 1 }}
      CreatedBy: ansible
      Environment: study
  with_items: "{{ ec2_instances_workers.instances }}"

- name: "Tag instance(s) etcds"
  amazon.aws.ec2_tag:
    region: "{{ region }}"
    resource: "{{ item.instance_id }}"
    profile: "{{ profile }}"
    state: present
    tags:
      Name: k8s-node-etcd-{{ item.ami_launch_index|int + 1 }}
      CreatedBy: ansible
      Environment: study
  with_items: "{{ ec2_instances_etcds.instances }}"

- name: Esperando o SSH dos masters
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html
  wait_for:
    host: "{{item.public_ip_address}}"
    port: 22
    state: started
  with_items: "{{ ec2_instances_masters.instances }}"

- name: Esperando ssh dos workers
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html
  wait_for:
    host: "{{item.public_ip_address}}"
    port: 22
    state: started
  with_items: "{{ ec2_instances_workers.instances }}"

- name: Esperando ssh dos etcds
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html
  wait_for:
    host: "{{item.public_ip_address}}"
    port: 22
    state: started
  with_items: "{{ ec2_instances_etcds.instances }}"

- name: Adicionando as instancias masters em memoria
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/add_host_module.html
  add_host: 
    name: "{{ item.public_ip_address }}"
    groups: masters
  with_items: "{{ ec2_instances_masters.instances }}"
  when: ec2_instances_masters

- name: Adicionando as instances workers em memória
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/add_host_module.html
  add_host: 
    name: "{{ item.public_ip_address }}"
    groups: workers
  with_items: "{{ ec2_instances_workers.instances }}"
  when: ec2_instances_workers

- name: Adicionando as instances etcds em memória
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/add_host_module.html
  add_host: 
    name: "{{ item.public_ip_address }}"
    groups: etcds
  with_items: "{{ ec2_instances_etcds.instances }}"
  when: ec2_instances_etcds
