# Aula 2

Vamos precisar do ambiente da aula 1 montado

## AD-HOC vs Playbooks

Ad-hoc é quando vc executa um comando direto via cli na maquina de destino ou um grupo de maquinas. Porém vc só consegue executar um comando de cada vez. Cada comando executado no cli tem os seus parametros, e parametros são chave valor certo? Quando colocamos esses parametros chave e valor dentro de um aquivo yml, ele se transforma em um playbook que na verdade nada mais é do que varios comandos ad-hoc em sequência já definido dentro do arquivo.

## Inventario -i

O inventário é o arquivo que defini os hosts. Vc pode ter vários arquivos de hosts inclusive separados por grupos e roles que veremos mais pra frente. No comando ad-hoc o path desse arquivo deve ser passado pelo parametro **-i**. Pode ter tanto o nome que o que o ansible consiga resolver ou um ip direto. Se usar o nome ele tem que estar definido em /etc/hosts de onde o ansible esta executando. Por padrão, caso vc não passe o inventário ele irá tentar ler o que estiver em /etc/ansible/hosts
<https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html>

## Módulos -m

[Módulos](https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html) são como funções que o ansible pode executar no dispositivo de destino. Tem módulo pra tudo que vc possa imaginar. Se funções em linguagem de programação recebe parâmtros os módulos também recebem.

o comando **ansible-doc** ajuda a ver a documentaçao dos modulos no cli

## Módulos Arguments -a

Se um módulo tiver argumentos ele deve ser passado com -a. Dentro da string deve ter todos os argumentos em chave e valor. \
Exemplo: -a "arg1=x arg2=y arg3=z"

## User -u

é um paramtro importante falar que representa qual usuário ele vai logar no ssh do dispositivo de destino. Se o usuário da máquina local for o mesmo que da máquina destino não precisa passar, mas é boa prãtica sempre passar para evitar qualquer transtorno em automações futuras.

## Password -k

Caso não esteja logando com a chave privada é possivel passar a senha também com esse parametro.

## Became -b

Se vc passar -b em uma chamada ansible significa que ele vai executar um sudo

## Testando um comando ad-hoc com o módulo Ping

Nesse caso foi feito o comando em todos os dispositivos que estao na no inventario hosts.
<https://docs.ansible.com/ansible/2.3/ping_module.htm>

```bash
[david@david-bon Aula 2]$ ansible -i hosts all -m ping -u ubuntu
robot-2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
robot-1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
[david@david-bon Aula 2]$ 
```

## Testando um comando ad-hoc com o módulo Apt

<https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#synop>

observe que se executarmos sem o -b da erro de permissão pois não usamos o sudo. Executamos esse comando domente no robot-1.

```bash
 [david@david-bon Aula 2]$ ansible -i hosts robot-1 -m apt -a "name=tmux state=present" -u ubuntu
robot-1 | FAILED! => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "msg": "'/usr/bin/apt-mark manual tmux' failed: E: Could not create temporary file for /var/lib/apt/extended_states - mkstemp (13: Permission denied)\nE: Failed to write temporary StateFile /var/lib/apt/extended_states\n",
    "rc": 100,
    "stderr": "E: Could not create temporary file for /var/lib/apt/extended_states - mkstemp (13: Permission denied)\nE: Failed to write temporary StateFile /var/lib/apt/extended_states\n",
    "stderr_lines": [
        "E: Could not create temporary file for /var/lib/apt/extended_states - mkstemp (13: Permission denied)",
        "E: Failed to write temporary StateFile /var/lib/apt/extended_states"
    ],
    "stdout": "",
    "stdout_lines": []
}
# Como sudo agora
[david@david-bon Aula 2]$ ansible -i hosts robot-1 -m apt -a "name=tmux state=present" -u ubuntu -b
robot-1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "cache_update_time": 1650550104,
    "cache_updated": false,
    "changed": false
}
```

### Fazendo um apt-get update

```bash
[david@david-bon Aula 2]$ ansible -i hosts robot-2 -m apt -a "update_cache=yes" -u ubuntu -b
robot-2 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "cache_update_time": 1652767479,
    "cache_updated": true,
    "changed": true
}
[david@david-bon Aula 2]$
```

### Instalando o htop

```bash
[david@david-bon Aula 2]$ ansible -i hosts robot-1 -m apt -a "update_cache=yes name=htop state=present" -u ubuntu -b
robot-1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "cache_update_time": 1652767258,
    "cache_updated": true,
    "changed": false
}
[david@david-bon Aula 2]$
```

## Modulo shell

<https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html>
Módulo mais usado e somente recebe um parametro de entrada que é a string que vc vai executar.

```bash
[david@david-bon Aula 2]$ ansible -i hosts all -m shell -a "ip address show" -u ubuntu
robot-2 | CHANGED | rc=0 >>
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9001 qdisc fq_codel state UP group default qlen 1000
    link/ether 02:5e:bf:13:b8:9e brd ff:ff:ff:ff:ff:ff
    inet 172.31.9.91/20 metric 100 brd 172.31.15.255 scope global dynamic eth0
       valid_lft 2223sec preferred_lft 2223sec
    inet6 fe80::5e:bfff:fe13:b89e/64 scope link 
       valid_lft forever preferred_lft forever
robot-1 | CHANGED | rc=0 >>
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9001 qdisc fq_codel state UP group default qlen 1000
    link/ether 02:f4:92:ae:a5:ae brd ff:ff:ff:ff:ff:ff
    inet 172.31.8.74/20 metric 100 brd 172.31.15.255 scope global dynamic eth0
       valid_lft 2909sec preferred_lft 2909sec
    inet6 fe80::f4:92ff:feae:a5ae/64 scope link 
       valid_lft forever preferred_lft forever
[david@david-bon Aula 2]$
```

### Modulo Shell vs. Módulo de Command

Na maioria dos casos de uso, os módulos de shell e de comando executam a mesma tarefa. No entanto, existem algumas diferenças principais entre esses dois módulos.

Ao usar o módulo **command**, os comandos não serão executados por meio de um shell. Consequentemente, variáveis ​​como $HOME não estão disponíveis e operações como, | e & não funcionará neste caso. Se você deseja usar essas variáveis ​​e operações, você precisa usar um módulo de shell.

Se você deseja executar um comando de maneira previsível e segura, é recomendável usar o módulo de command em vez do shell. O módulo de comando não será afetado pelo ambiente do usuário, então é mais seguro do que o módulo shell.

## Modulo Copy

<https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html>
copiando o arquivo file_teste para dentro de /tmp e mostrando depois. Porém alterei o .cfg do ansible que esta nesta pasta definindo o user o inventario e a chave privada para não precisar fazer o ssh-add etc. Logo o comando reduziu.

```bash
~/projects/ansible/study-ansible/Aula 2 main !2 ?2                                                                                                                            1.1.7 13:46:26
❯ ansible robot-1 -m copy -a "src=file_teste dest=/tmp"
robot-1 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": true,
    "checksum": "e8d25c415a1ec088b1e714b4e08776be6d06ea26",
    "dest": "/tmp/file_teste",
    "gid": 1000,
    "group": "ubuntu",
    "md5sum": "e305f540318571b54667b25010303db9",
    "mode": "0664",
    "owner": "ubuntu",
    "size": 10,
    "src": "/home/ubuntu/.ansible/tmp/ansible-tmp-1652806120.255548-14461-131398014382363/source",
    "state": "file",
    "uid": 1000
}


❯ ansible robot-1 -m shell -a "cat /tmp/file_teste"
robot-1 | CHANGED | rc=0 >>
teste copy
```

## Modulo Git

<https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html>

Vamos clonar o repo do próprio ansible usando o modulo do git

```bash
~/projects/ansible/study-ansible/Aula 2 main !2 ?3                                                                                                                        6s  1.1.7 14:13:49
❯ ansible robot-2 -m git -a "repo=https://github.com/ansible/ansible.git dest=/home/ubuntu/repo_ansible"
robot-2 | CHANGED => {
    "after": "201aba3d0f6801471a3f9ef66eff2b7cc0d77b97",
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "before": null,
    "changed": true
}

~/projects/ansible/study-ansible/Aula 2 main !2 ?3                                                                                                                       36s  1.1.7 14:14:54
❯ ansible robot-2 -m shell -a "ls -lha /home/ubuntu/repo_ansible"                                       
robot-2 | CHANGED | rc=0 >>
total 144K
drwxrwxr-x 14 ubuntu ubuntu 4.0K May 17 17:14 .
drwxr-x---  6 ubuntu ubuntu 4.0K May 17 17:14 ..
drwxrwxr-x  4 ubuntu ubuntu 4.0K May 17 17:14 .azure-pipelines
-rw-rw-r--  1 ubuntu ubuntu  202 May 17 17:14 .cherry_picker.toml
drwxrwxr-x  8 ubuntu ubuntu 4.0K May 17 17:14 .git
-rw-rw-r--  1 ubuntu ubuntu   23 May 17 17:14 .gitattributes
drwxrwxr-x  3 ubuntu ubuntu 4.0K May 17 17:14 .github
-rw-rw-r--  1 ubuntu ubuntu 2.8K May 17 17:14 .gitignore
-rw-rw-r--  1 ubuntu ubuntu 2.2K May 17 17:14 .mailmap
-rw-rw-r--  1 ubuntu ubuntu  35K May 17 17:14 COPYING
-rw-rw-r--  1 ubuntu ubuntu 2.5K May 17 17:14 MANIFEST.in
-rw-rw-r--  1 ubuntu ubuntu 4.9K May 17 17:14 Makefile
-rw-rw-r--  1 ubuntu ubuntu 5.6K May 17 17:14 README.rst
drwxrwxr-x  2 ubuntu ubuntu 4.0K May 17 17:14 bin
drwxrwxr-x  3 ubuntu ubuntu 4.0K May 17 17:14 changelogs
drwxrwxr-x  6 ubuntu ubuntu 4.0K May 17 17:14 docs
drwxrwxr-x  3 ubuntu ubuntu 4.0K May 17 17:14 examples
drwxrwxr-x  7 ubuntu ubuntu 4.0K May 17 17:14 hacking
drwxrwxr-x  3 ubuntu ubuntu 4.0K May 17 17:14 lib
drwxrwxr-x  2 ubuntu ubuntu 4.0K May 17 17:14 licenses
drwxrwxr-x  4 ubuntu ubuntu 4.0K May 17 17:14 packaging
-rw-rw-r--  1 ubuntu ubuntu  100 May 17 17:14 pyproject.toml
-rw-rw-r--  1 ubuntu ubuntu  653 May 17 17:14 requirements.txt
-rw-rw-r--  1 ubuntu ubuntu 2.4K May 17 17:14 setup.cfg
-rw-rw-r--  1 ubuntu ubuntu 1.1K May 17 17:14 setup.py
drwxrwxr-x  9 ubuntu ubuntu 4.0K May 17 17:14 test
```

## Modulo Setup

<https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html>

Este módulo é chamado automaticamente por playbooks para reunir variáveis ​​úteis sobre hosts remotos. É considero um dos mais importante de todos e utilizado em demasia.

```bash
~/projects/ansible/study-ansible/Aula 2 main !2 ?3                                                                                                                            1.1.7 14:53:29
❯ ansible robot-1 -m setup                                        
robot-1 | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "172.31.8.74"
        ],
        "ansible_all_ipv6_addresses": [
            "fe80::f4:92ff:feae:a5ae"
        ],
        "ansible_apparmor": {
            "status": "enabled"
        },
        # Arquitetura
        "ansible_architecture": "x86_64",
        "ansible_bios_date": "08/24/2006",
        "ansible_bios_vendor": "Xen",
        "ansible_bios_version": "4.11.amazon",
        "ansible_board_asset_tag": "NA",
        "ansible_board_name": "NA",
        "ansible_board_serial": "NA",
        "ansible_board_vendor": "NA",
        "ansible_board_version": "NA",
        "ansible_chassis_asset_tag": "NA",
        "ansible_chassis_serial": "NA",
        "ansible_chassis_vendor": "Xen",
        "ansible_chassis_version": "NA",
        "ansible_cmdline": {
            "BOOT_IMAGE": "/boot/vmlinuz-5.15.0-1004-aws",
            "console": "ttyS0",
            "nvme_core.io_timeout": "4294967295",
            "panic": "-1",
            "ro": true,
            "root": "PARTUUID=6a77fd29-0842-4a2a-b312-cb1d29398c98"
        },
        "ansible_date_time": {
            "date": "2022-05-17",
            "day": "17",
            "epoch": "1652810028",
            "hour": "17",
            "iso8601": "2022-05-17T17:53:48Z",
            "iso8601_basic": "20220517T175348411733",
            "iso8601_basic_short": "20220517T175348",
            "iso8601_micro": "2022-05-17T17:53:48.411733Z",
            "minute": "53",
            "month": "05",
            "second": "48",
            "time": "17:53:48",
            "tz": "UTC",
            "tz_offset": "+0000",
            "weekday": "Tuesday",
            "weekday_number": "2",
            "weeknumber": "20",
            "year": "2022"
        },
        "ansible_default_ipv4": {
            ## INFORMACOES DE REDE
            "address": "172.31.8.74",
            "alias": "eth0",
            "broadcast": "",
            "gateway": "172.31.0.1",
            "interface": "eth0",
            "macaddress": "02:f4:92:ae:a5:ae",
            "mtu": 9001,
            "netmask": "255.255.240.0",
            "network": "172.31.0.0",
            "type": "ether"
        },
        "ansible_default_ipv6": {},
        "ansible_device_links": {
            "ids": {},
            "labels": {
                "xvda1": [
                    "cloudimg-rootfs"
                ],
                "xvda15": [
                    "UEFI"
                ]
            },
            "masters": {},
            "uuids": {
                "xvda1": [
                    "e7879b8a-f914-4210-998a-d47604682e59"
                ],
                "xvda15": [
                    "594C-4810"
                ]
            }
        },
        "ansible_devices": {
            "loop0": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "54552",
                "sectorsize": "512",
                "size": "26.64 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop1": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "113696",
                "sectorsize": "512",
                "size": "55.52 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop2": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "126760",
                "sectorsize": "512",
                "size": "61.89 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop3": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "163736",
                "sectorsize": "512",
                "size": "79.95 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop4": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "89352",
                "sectorsize": "512",
                "size": "43.63 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop5": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "91496",
                "sectorsize": "512",
                "size": "44.68 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop6": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "113736",
                "sectorsize": "512",
                "size": "55.54 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop7": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "126776",
                "sectorsize": "512",
                "size": "61.90 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "loop8": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "none",
                "sectors": "51416",
                "sectorsize": "512",
                "size": "25.11 MB",
                "support_discard": "4096",
                "vendor": null,
                "virtual": 1
            },
            "xvda": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": null,
                "partitions": {
                    "xvda1": {
                        "holders": [],
                        "links": {
                            "ids": [],
                            "labels": [
                                "cloudimg-rootfs"
                            ],
                            "masters": [],
                            "uuids": [
                                "e7879b8a-f914-4210-998a-d47604682e59"
                            ]
                        },
                        "sectors": "16549855",
                        "sectorsize": 512,
                        "size": "7.89 GB",
                        "start": "227328",
                        "uuid": "e7879b8a-f914-4210-998a-d47604682e59"
                    },
                    "xvda14": {
                        "holders": [],
                        "links": {
                            "ids": [],
                            "labels": [],
                            "masters": [],
                            "uuids": []
                        },
                        "sectors": "8192",
                        "sectorsize": 512,
                        "size": "4.00 MB",
                        "start": "2048",
                        "uuid": null
                    },
                    "xvda15": {
                        "holders": [],
                        "links": {
                            "ids": [],
                            "labels": [
                                "UEFI"
                            ],
                            "masters": [],
                            "uuids": [
                                "594C-4810"
                            ]
                        },
                        "sectors": "217088",
                        "sectorsize": 512,
                        "size": "106.00 MB",
                        "start": "10240",
                        "uuid": "594C-4810"
                    }
                },
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "mq-deadline",
                "sectors": "16777216",
                "sectorsize": "512",
                "size": "8.00 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            }
        },
        "ansible_distribution": "Ubuntu",
        "ansible_distribution_file_parsed": true,
        "ansible_distribution_file_path": "/etc/os-release",
        "ansible_distribution_file_variety": "Debian",
        "ansible_distribution_major_version": "22",
        "ansible_distribution_release": "jammy",
        "ansible_distribution_version": "22.04",
        "ansible_dns": {
            "nameservers": [
                "127.0.0.53"
            ],
            "options": {
                "edns0": true,
                "trust-ad": true
            },
            "search": [
                "us-east-2.compute.internal"
            ]
        },
        ## domain
        "ansible_domain": "us-east-2.compute.internal",
        "ansible_effective_group_id": 1000,
        "ansible_effective_user_id": 1000,
        "ansible_env": {
            "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus",
            "HOME": "/home/ubuntu",
            "LANG": "C.UTF-8",
            "LOGNAME": "ubuntu",
            "MOTD_SHOWN": "pam",
            "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin",
            "PWD": "/home/ubuntu",
            "SHELL": "/bin/bash",
            "SHLVL": "0",
            "SSH_CLIENT": "177.137.230.216 17312 22",
            "SSH_CONNECTION": "177.137.230.216 17312 172.31.8.74 22",
            "SSH_TTY": "/dev/pts/0",
            "TERM": "xterm-256color",
            #nome do user
            "USER": "ubuntu",
            "XDG_RUNTIME_DIR": "/run/user/1000",
            "XDG_SESSION_CLASS": "user",
            "XDG_SESSION_ID": "38",
            "XDG_SESSION_TYPE": "tty",
            "_": "/bin/sh"
        },
        "ansible_eth0": {
            "active": true,
            "device": "eth0",
            "features": {
                "esp_hw_offload": "off [fixed]",
                "esp_tx_csum_hw_offload": "off [fixed]",
                "fcoe_mtu": "off [fixed]",
                "generic_receive_offload": "on",
                "generic_segmentation_offload": "on",
                "highdma": "off [fixed]",
                "hsr_dup_offload": "off [fixed]",
                "hsr_fwd_offload": "off [fixed]",
                "hsr_tag_ins_offload": "off [fixed]",
                "hsr_tag_rm_offload": "off [fixed]",
                "hw_tc_offload": "off [fixed]",
                "l2_fwd_offload": "off [fixed]",
                "large_receive_offload": "off [fixed]",
                "loopback": "off [fixed]",
                "macsec_hw_offload": "off [fixed]",
                "netns_local": "off [fixed]",
                "ntuple_filters": "off [fixed]",
                "receive_hashing": "off [fixed]",
                "rx_all": "off [fixed]",
                "rx_checksumming": "on [fixed]",
                "rx_fcs": "off [fixed]",
                "rx_gro_hw": "off [fixed]",
                "rx_gro_list": "off",
                "rx_udp_gro_forwarding": "off",
                "rx_udp_tunnel_port_offload": "off [fixed]",
                "rx_vlan_filter": "off [fixed]",
                "rx_vlan_offload": "off [fixed]",
                "rx_vlan_stag_filter": "off [fixed]",
                "rx_vlan_stag_hw_parse": "off [fixed]",
                "scatter_gather": "on",
                "tcp_segmentation_offload": "on",
                "tls_hw_record": "off [fixed]",
                "tls_hw_rx_offload": "off [fixed]",
                "tls_hw_tx_offload": "off [fixed]",
                "tx_checksum_fcoe_crc": "off [fixed]",
                "tx_checksum_ip_generic": "off [fixed]",
                "tx_checksum_ipv4": "on [fixed]",
                "tx_checksum_ipv6": "on",
                "tx_checksum_sctp": "off [fixed]",
                "tx_checksumming": "on",
                "tx_esp_segmentation": "off [fixed]",
                "tx_fcoe_segmentation": "off [fixed]",
                "tx_gre_csum_segmentation": "off [fixed]",
                "tx_gre_segmentation": "off [fixed]",
                "tx_gso_list": "off [fixed]",
                "tx_gso_partial": "off [fixed]",
                "tx_gso_robust": "on [fixed]",
                "tx_ipxip4_segmentation": "off [fixed]",
                "tx_ipxip6_segmentation": "off [fixed]",
                "tx_lockless": "off [fixed]",
                "tx_nocache_copy": "off",
                "tx_scatter_gather": "on",
                "tx_scatter_gather_fraglist": "off [fixed]",
                "tx_sctp_segmentation": "off [fixed]",
                "tx_tcp6_segmentation": "on",
                "tx_tcp_ecn_segmentation": "off [fixed]",
                "tx_tcp_mangleid_segmentation": "off",
                "tx_tcp_segmentation": "on",
                "tx_tunnel_remcsum_segmentation": "off [fixed]",
                "tx_udp_segmentation": "off [fixed]",
                "tx_udp_tnl_csum_segmentation": "off [fixed]",
                "tx_udp_tnl_segmentation": "off [fixed]",
                "tx_vlan_offload": "off [fixed]",
                "tx_vlan_stag_hw_insert": "off [fixed]",
                "vlan_challenged": "off [fixed]"
            },
            "hw_timestamp_filters": [],
            "ipv4": {
                "address": "172.31.8.74",
                "broadcast": "",
                "netmask": "255.255.240.0",
                "network": "172.31.0.0"
            },
            "ipv6": [
                {
                    "address": "fe80::f4:92ff:feae:a5ae",
                    "prefix": "64",
                    "scope": "link"
                }
            ],
            "macaddress": "02:f4:92:ae:a5:ae",
            "module": "xen_netfront",
            "mtu": 9001,
            "pciid": "vif-0",
            "promisc": false,
            "timestamping": [],
            "type": "ether"
        },
        "ansible_fibre_channel_wwn": [],
        "ansible_fips": false,
        "ansible_form_factor": "Other",
        "ansible_fqdn": "ip-172-31-8-74.us-east-2.compute.internal",
        "ansible_hostname": "ip-172-31-8-74",
        "ansible_hostnqn": "",
        "ansible_interfaces": [
            "lo",
            "eth0"
        ],
        "ansible_is_chroot": false,
        "ansible_iscsi_iqn": "",
        "ansible_kernel": "5.15.0-1004-aws",
        "ansible_kernel_version": "#6-Ubuntu SMP Thu Mar 31 09:44:20 UTC 2022",
        "ansible_lo": {
            "active": true,
            "device": "lo",
            "features": {
                "esp_hw_offload": "off [fixed]",
                "esp_tx_csum_hw_offload": "off [fixed]",
                "fcoe_mtu": "off [fixed]",
                "generic_receive_offload": "on",
                "generic_segmentation_offload": "on",
                "highdma": "on [fixed]",
                "hsr_dup_offload": "off [fixed]",
                "hsr_fwd_offload": "off [fixed]",
                "hsr_tag_ins_offload": "off [fixed]",
                "hsr_tag_rm_offload": "off [fixed]",
                "hw_tc_offload": "off [fixed]",
                "l2_fwd_offload": "off [fixed]",
                "large_receive_offload": "off [fixed]",
                "loopback": "on [fixed]",
                "macsec_hw_offload": "off [fixed]",
                "netns_local": "on [fixed]",
                "ntuple_filters": "off [fixed]",
                "receive_hashing": "off [fixed]",
                "rx_all": "off [fixed]",
                "rx_checksumming": "on [fixed]",
                "rx_fcs": "off [fixed]",
                "rx_gro_hw": "off [fixed]",
                "rx_gro_list": "off",
                "rx_udp_gro_forwarding": "off",
                "rx_udp_tunnel_port_offload": "off [fixed]",
                "rx_vlan_filter": "off [fixed]",
                "rx_vlan_offload": "off [fixed]",
                "rx_vlan_stag_filter": "off [fixed]",
                "rx_vlan_stag_hw_parse": "off [fixed]",
                "scatter_gather": "on",
                "tcp_segmentation_offload": "on",
                "tls_hw_record": "off [fixed]",
                "tls_hw_rx_offload": "off [fixed]",
                "tls_hw_tx_offload": "off [fixed]",
                "tx_checksum_fcoe_crc": "off [fixed]",
                "tx_checksum_ip_generic": "on [fixed]",
                "tx_checksum_ipv4": "off [fixed]",
                "tx_checksum_ipv6": "off [fixed]",
                "tx_checksum_sctp": "on [fixed]",
                "tx_checksumming": "on",
                "tx_esp_segmentation": "off [fixed]",
                "tx_fcoe_segmentation": "off [fixed]",
                "tx_gre_csum_segmentation": "off [fixed]",
                "tx_gre_segmentation": "off [fixed]",
                "tx_gso_list": "on",
                "tx_gso_partial": "off [fixed]",
                "tx_gso_robust": "off [fixed]",
                "tx_ipxip4_segmentation": "off [fixed]",
                "tx_ipxip6_segmentation": "off [fixed]",
                "tx_lockless": "on [fixed]",
                "tx_nocache_copy": "off [fixed]",
                "tx_scatter_gather": "on [fixed]",
                "tx_scatter_gather_fraglist": "on [fixed]",
                "tx_sctp_segmentation": "on",
                "tx_tcp6_segmentation": "on",
                "tx_tcp_ecn_segmentation": "on",
                "tx_tcp_mangleid_segmentation": "on",
                "tx_tcp_segmentation": "on",
                "tx_tunnel_remcsum_segmentation": "off [fixed]",
                "tx_udp_segmentation": "on",
                "tx_udp_tnl_csum_segmentation": "off [fixed]",
                "tx_udp_tnl_segmentation": "off [fixed]",
                "tx_vlan_offload": "off [fixed]",
                "tx_vlan_stag_hw_insert": "off [fixed]",
                "vlan_challenged": "on [fixed]"
            },
            "hw_timestamp_filters": [],
            "ipv4": {
                "address": "127.0.0.1",
                "broadcast": "",
                "netmask": "255.0.0.0",
                "network": "127.0.0.0"
            },
            "ipv6": [
                {
                    "address": "::1",
                    "prefix": "128",
                    "scope": "host"
                }
            ],
            "mtu": 65536,
            "promisc": false,
            "timestamping": [],
            "type": "loopback"
        },
        "ansible_local": {},
        "ansible_lsb": {
            "codename": "jammy",
            "description": "Ubuntu 22.04 LTS",
            "id": "Ubuntu",
            "major_release": "22",
            "release": "22.04"
        },
        "ansible_machine": "x86_64",
        "ansible_machine_id": "cb7a414cdb574d2cbe0f54770bcbefaa",
        "ansible_memfree_mb": 467,
        "ansible_memory_mb": {
            "nocache": {
                "free": 1673,
                "used": 301
            },
            "real": {
                "free": 467,
                "total": 1974,
                "used": 1507
            },
            "swap": {
                "cached": 0,
                "free": 0,
                "total": 0,
                "used": 0
            }
        },
        "ansible_memtotal_mb": 1974,
        "ansible_mounts": [
            {
                "block_available": 1438152,
                "block_size": 4096,
                "block_total": 1987689,
                "block_used": 549537,
                "device": "/dev/root",
                "fstype": "ext4",
                "inode_available": 930158,
                "inode_total": 1032192,
                "inode_used": 102034,
                "mount": "/",
                "options": "rw,relatime,discard,errors=remount-ro",
                "size_available": 5890670592,
                "size_total": 8141574144,
                "uuid": "e7879b8a-f914-4210-998a-d47604682e59"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 214,
                "block_used": 214,
                "device": "/dev/loop0",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 16,
                "inode_used": 16,
                "mount": "/snap/amazon-ssm-agent/5163",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 28049408,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 445,
                "block_used": 445,
                "device": "/dev/loop1",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 10849,
                "inode_used": 10849,
                "mount": "/snap/core18/2344",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 58327040,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 350,
                "block_used": 350,
                "device": "/dev/loop4",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 482,
                "inode_used": 482,
                "mount": "/snap/snapd/15177",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 45875200,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 496,
                "block_used": 496,
                "device": "/dev/loop2",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 11778,
                "inode_used": 11778,
                "mount": "/snap/core20/1405",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 65011712,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 640,
                "block_used": 640,
                "device": "/dev/loop3",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 816,
                "inode_used": 816,
                "mount": "/snap/lxd/22923",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 83886080,
                "uuid": "N/A"
            },
            {
                "block_available": 203058,
                "block_size": 512,
                "block_total": 213716,
                "block_used": 10658,
                "device": "/dev/xvda15",
                "fstype": "vfat",
                "inode_available": 0,
                "inode_total": 0,
                "inode_used": 0,
                "mount": "/boot/efi",
                "options": "rw,relatime,fmask=0077,dmask=0077,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro",
                "size_available": 103965696,
                "size_total": 109422592,
                "uuid": "594C-4810"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 358,
                "block_used": 358,
                "device": "/dev/loop5",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 484,
                "inode_used": 484,
                "mount": "/snap/snapd/15534",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 46923776,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 445,
                "block_used": 445,
                "device": "/dev/loop6",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 10857,
                "inode_used": 10857,
                "mount": "/snap/core18/2409",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 58327040,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 496,
                "block_used": 496,
                "device": "/dev/loop7",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 11789,
                "inode_used": 11789,
                "mount": "/snap/core20/1434",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 65011712,
                "uuid": "N/A"
            },
            {
                "block_available": 0,
                "block_size": 131072,
                "block_total": 201,
                "block_used": 201,
                "device": "/dev/loop8",
                "fstype": "squashfs",
                "inode_available": 0,
                "inode_total": 16,
                "inode_used": 16,
                "mount": "/snap/amazon-ssm-agent/5656",
                "options": "ro,nodev,relatime,errors=continue",
                "size_available": 0,
                "size_total": 26345472,
                "uuid": "N/A"
            }
        ],
        "ansible_nodename": "ip-172-31-8-74",
        "ansible_os_family": "Debian",
        "ansible_pkg_mgr": "apt",
        "ansible_proc_cmdline": {
            "BOOT_IMAGE": "/boot/vmlinuz-5.15.0-1004-aws",
            "console": [
                "tty1",
                "ttyS0"
            ],
            "nvme_core.io_timeout": "4294967295",
            "panic": "-1",
            "ro": true,
            "root": "PARTUUID=6a77fd29-0842-4a2a-b312-cb1d29398c98"
        },
        "ansible_processor": [
            "0",
            "GenuineIntel",
            "Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz"
        ],
        "ansible_processor_cores": 1,
        "ansible_processor_count": 1,
        "ansible_processor_nproc": 1,
        "ansible_processor_threads_per_core": 1,
        "ansible_processor_vcpus": 1,
        "ansible_product_name": "HVM domU",
        "ansible_product_serial": "NA",
        "ansible_product_uuid": "NA",
        "ansible_product_version": "4.11.amazon",
        "ansible_python": {
            "executable": "/usr/bin/python3",
            "has_sslcontext": true,
            "type": "cpython",
            "version": {
                "major": 3,
                "micro": 4,
                "minor": 10,
                "releaselevel": "final",
                "serial": 0
            },
            "version_info": [
                3,
                10,
                4,
                "final",
                0
            ]
        },
        "ansible_python_version": "3.10.4",
        "ansible_real_group_id": 1000,
        "ansible_real_user_id": 1000,
        "ansible_selinux": {
            "status": "Missing selinux Python library"
        },
        "ansible_selinux_python_present": false,
        "ansible_service_mgr": "systemd",
        "ansible_ssh_host_key_dsa_public": "AAAAB3NzaC1kc3MAAACBAOse7/QvTD7sL0iL30ALP0m7MMqZthT4+NYjPuS4/ZQmjC2Lkp2/+PPf292/4uhYwNAogchFGyLVcX/2xfngNJKK28WmSAK7nTbkfJg6FKu3AQ+LymzhWPmZYA5PSY2z4Rz8nvnyFK/t56RcwmGZwzu1tapPrOuEfD713hk0oi//AAAAFQCs+qdkN8JhrD7C5qdwVTGgHLevoQAAAIAAn5cU22Ao7D6eLAwTFN9HEDs2jWkPwCLqwO2rYKhGJ/2L49pVeVjG9XYMTj+F9iLQiqmun9tP7VDBi5bUKZqyelxL51JAUTbAbxcA+2JmXQpAcTwMFuLCNq//WAnRqtcNTkBqfqf6M20CwJUX0lwaJcip1GT+JIn8bj4VJBw3mwAAAIEAgyTU8ybQWt9SsPv+WHvt4GKkrf1am7XplS+FjoNcOvef7PbE7WhZcZot1E4zZQYfSL4kgyX2NK3+K0q3HK2Yau/hUuIHme/9zRRdwaAhhdBKQR72q52TdOhO+ccdlvOjQ8N7oJKcU8A92L8FOwN0ikY5gDwqowWEAE2zkkWvAOQ=",
        "ansible_ssh_host_key_dsa_public_keytype": "ssh-dss",
        "ansible_ssh_host_key_ecdsa_public": "AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAnfZ2WTONzzgLMFeK/Z14tI94HpVhW/XCQJnvb/huZo4BuNfAFbO3VHmmM+RqO4PPu+pxrMGvG4KWWldWayGNg=",
        "ansible_ssh_host_key_ecdsa_public_keytype": "ecdsa-sha2-nistp256",
        "ansible_ssh_host_key_ed25519_public": "AAAAC3NzaC1lZDI1NTE5AAAAIG88WZR+FN4depPKxtypOiioF2F0NgrwUGedUr4c7iYN",
        "ansible_ssh_host_key_ed25519_public_keytype": "ssh-ed25519",
        "ansible_ssh_host_key_rsa_public": "AAAAB3NzaC1yc2EAAAADAQABAAABgQDKCSd1BwTJ5iFatksTiYUX6zKv7LakoK6aY8f75eBPKSPp9gkj8zT9nKJKtdDn9NtXn/2zqIYJmuNWnEZLaUJ9542dcCS0c7wOo01kakA1LcoT+5HMgfvzHGqDL4A9du0iS9maGy1Vz1qcWwa34HtEYdnq4k+TssvhlJb2ocbfF3srFEB2aYg0PBY3TEJalPb2IC+4eXeQTXggAQrJcymkxNPWqRRuly+pNhuA9E4i40x4ay1jl8dElsg5LkhsC52nz1vyJCRn4MiOV6afvYwEiHCOzdR1cly83/PaL+vO8xWHYQ3S9GYdZfmR5yajvfeb7fGRGOvkp0FO1jGZ55KRDYKqVMWMe0wLghVJsmWyhcE3gD/u0Gpz29HJOJtIkfqK4mRSNfGdD0lWeeMlAU3MV3uiKVFyCeuFyAL9nmFYew5+h6Dd88k673kyAP8Wd1OFnBdzW/ucqDYFGUVMcJC0SccXxD0kbHvuYzriAAbzpUuQnsnaFRGc7FeSZ9hdJ5M=",
        "ansible_ssh_host_key_rsa_public_keytype": "ssh-rsa",
        "ansible_swapfree_mb": 0,
        "ansible_swaptotal_mb": 0,
        "ansible_system": "Linux",
        "ansible_system_capabilities": [
            ""
        ],
        "ansible_system_capabilities_enforced": "True",
        "ansible_system_vendor": "Xen",
        "ansible_uptime_seconds": 52276,
        "ansible_user_dir": "/home/ubuntu",
        "ansible_user_gecos": "Ubuntu",
        "ansible_user_gid": 1000,
        "ansible_user_id": "ubuntu",
        "ansible_user_shell": "/bin/bash",
        "ansible_user_uid": 1000,
        "ansible_userspace_architecture": "x86_64",
        "ansible_userspace_bits": "64",
        "ansible_virtualization_role": "guest",
        "ansible_virtualization_type": "xen",
        "discovered_interpreter_python": "/usr/bin/python3",
        "gather_subset": [
            "all"
        ],
        "module_setup": true
    },
    "changed": false
}

```

```bash
~/projects/ansible/study-ansible/Aula 2 main !2 ?3                                                                                                                            1.1.7 14:59:46
❯ ansible all -m setup -a "filter=ansible_distribution" 
robot-2 | SUCCESS => {
    "ansible_facts": {
        "ansible_distribution": "Ubuntu",
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false
}
robot-1 | SUCCESS => {
    "ansible_facts": {
        "ansible_distribution": "Ubuntu",
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false
}

~/projects/ansible/study-ansible/Aula 2 main !2 ?3                                                                                                                        8s  1.1.7 15:01:40
❯ ansible all -m setup -a "filter=ansible_nodename"    
robot-1 | SUCCESS => {
    "ansible_facts": {
        "ansible_nodename": "ip-172-31-8-74",
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false
}
robot-2 | SUCCESS => {
    "ansible_facts": {
        "ansible_nodename": "ip-172-31-9-91",
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false
}
```
