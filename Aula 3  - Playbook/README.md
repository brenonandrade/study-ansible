# Aula 3

Vamos criar nosso primeiro playbook Simples.
Vamos instalar o nginx para teste.

## AD-HOC vs Playbooks

Comando ad-hoc são perdidos, e para isso que colocamos tudo em playbooks. Geralmente o uso do ansible deve sempre ser feito em cima de playbooks.

Já pensou vc ter que fazer comanado a comando de tudo aquilo que vc fez e nem consegue lembrar novamente ou reaproveitar o que foi feito?
Com um arquivo vc pode ter uma gerencia dos seus comandos e dar manutenção melhor no seu parque de máquinas.
Em caso de configuraçao sempre utilize playbook, em caso de consulta pode usar o ad-hoc.

Vamos alterar o host formando grupos agora.
Nesse caso toda vez que nos referenciarmos a webservers o ansible irá entrar nas duas maquinas, e db somente na robot-2

```yaml
[webservers]
robot-1
robot-2

[db]
robot-2
```

## Primeiro playbook

Veja [my_playbook](./my_playbook.yml) para ver o arquivo que vamos aplicar.

Observe que já colocamos o hosts que será executado a tarefa, logo nem precisamos passar o hosts
Outro detalhe é que passamos o remote_user e quadno essa varíavel é passada ela substitui a que esta definida no .cfg

```bash
~/projects/ansible/study-ansible/Aula 3  - Playbook main !3 ?3                                                                                                                     1.1.7 18:07:41
❯ ansible-playbook my_playbook.yml

PLAY [Install nginx] ******************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

TASK [Instalando o Nginx] *************************************************************************************************************************************************************************
changed: [robot-1]
changed: [robot-2]

PLAY RECAP ****************************************************************************************************************************************************************************************
robot-1                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
robot-2                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   


~/projects/ansible/study-ansible/Aula 3  - Playbook main !3 ?3                                                                                                                     1.1.7 18:10:13
❯ curl http://robot-1:    
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>


~/projects/ansible/study-ansible/Aula 3  - Playbook main !3 ?3                                                                                                                     1.1.7 18:11:18
❯ ansible webservers -m shell -a "ps -ef | grep nginx"
robot-2 | CHANGED | rc=0 >>
root       13122       1  0 21:08 ?        00:00:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data   13125   13122  0 21:08 ?        00:00:00 nginx: worker process
ubuntu     13314   13313  0 21:11 pts/0    00:00:00 /bin/sh -c ps -ef | grep nginx
ubuntu     13316   13314  0 21:11 pts/0    00:00:00 grep nginx
robot-1 | CHANGED | rc=0 >>
root       13755       1  0 21:08 ?        00:00:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data   13758   13755  0 21:08 ?        00:00:00 nginx: worker process
ubuntu     13947   13946  0 21:11 pts/0    00:00:00 /bin/sh -c ps -ef | grep nginx
ubuntu     13949   13947  0 21:11 pts/0    00:00:00 grep nginx
```

## Segundo playbook

Veja [my_playbook2](./my_playbook2.yml)
Nesse caso vamos usar o módulo de service para garantir que servico estará ativo e iniciado.
<https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html>

```bash
~/projects/ansible/study-ansible/Aula 3  - Playbook main !3 ?3                                                                                                                     1.1.7 18:20:51
❯ ansible-playbook my_playbook\ 2.yml 

PLAY [Install nginx] ******************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

TASK [Instalando o Nginx] *************************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

TASK [Habilitando service nginx] ******************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

TASK [Startando service nginx] ********************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

PLAY RECAP ****************************************************************************************************************************************************************************************
robot-1                    : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
robot-2                    : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

## Terceira versao playbook

Veja [my_playbook3](./my_playbook3.yml)
Vamos usar o módulo copy e um handler que é notificado para reiniciar o nginx quando um arquivo de configuracao for modificado.

Onde fica ok quer dizer que ele não alterou pois foram feitos pelos comandos anteriores, ou seja, ele nao reexecuta. Isso é a idempotencia.

```bash
❯ ansible-playbook my_playbook3.yml                      

PLAY [Install nginx] ******************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

TASK [Instalando o Nginx] *************************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

TASK [Habilitando service nginx] ******************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

TASK [Startando service nginx] ********************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

TASK [Copiando o index] ***************************************************************************************************************************************************************************
changed: [robot-2]
changed: [robot-1]

TASK [Copiando o nginx.conf] **********************************************************************************************************************************************************************
changed: [robot-2]
changed: [robot-1]

RUNNING HANDLER [Restantando o nginx] *************************************************************************************************************************************************************
changed: [robot-2]
changed: [robot-1]

PLAY RECAP ****************************************************************************************************************************************************************************************
robot-1                    : ok=7    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
robot-2                    : ok=7    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
 
```

## Extras

A execução de um playbook pode ser feita com alguns extras

Pode executar com vários níveis de verbosidade

````bash
ansible-playbook main.yml
ansible-playbook main.yml -v 
ansible-playbook main.yml -vv
ansible-playbook main.yml -vvv
ansible-playbook main.yml -vvvv
````

Serve passa ensaiar as mudancas e o que aconteceria, bom para uma situação destrutiva

````bash
ansible-playbook main.yml --check
````

Da para usar o diff para ver as diferençcas

````bash
ansible-playbook main.yml --diff
````

Da para executar chamando uma tag especifica para uma role ou tag para evitar rodar o arquivo inteiro

````bash
ansible-playbook main.yml --tag "install_role"
````

ou somando tudo

````bash
ansible-playbook main.yml --diff -vv --tag"install_role"
````
