# study-ansible

<img src="./pics/ansible.png" alt="drawing" width="200"/>

<https://docs.ansible.com/ansible/latest/index.html>

Ansible é uma ferramenta de automação criada para gerenciar várias mãquinas de uma vez só. É capaz de configurar sistemas, implantar softwares e orquestrar tarefas avançadas como implantação continua e atualização contínua.

- Muito popular entre os sysadmins e devops.

- Documentação muito completa, considerada uma das mais detalhadas existentes.

- Curva de aprendizado muito curta.

- Forte foco em segurança e simplicidade.

- Utiliza o protocolo ssh e uma linguagem yaml (simples).

- Fácil de realizar manutenção.

- Não necessita agents instalados nas instâncias, o que é o seu maior diferencial comparado ao [Chef](https://www.chef.io/) e ao [Chef](https://puppet.com/).

- Descentralizado, mas pode se conectar com Kerberos, LDAP e outros sistemas de autênticação centralizada.

- Bom pra gerenciar poucas ou muitas instâncias. Não necessáriamente somente mãquinas, mas qualquer coisa que aceite um ssh como um switch, roteador, load balancer, etc.

- Idepotencia, ou seja, garante que uma tarefa já executada näo seja executada novamente, o que é uma vantagem sobre script por exemplo.

- Uso de templates por substituição de variáveis usando o Jinja.

## Pros e Contras

- Não garante que a configuração esta ativa em todas as máquinas, logo usa-lo em um parque de máquinas não é a melhor opção. Claro que existem saídas para isso como a utilização do ansible tower que é pago para potencializar a utilizaçao dele, mas não é a melhor escolha.

- Bom para ser somado a outras ferramentas como por exemplo o terraform.

- Uma coisa muito interessante é que precisamos trabalhar com idempotência. Isso significa que se executar um script várias vezes o resultado será sempre o mesmo. Por exemplo se vc pediu para criar um usuário rodando um script e depois rodout novamente o script ele não irá criar duas vezes.

## Pastas

A pasta raiz desse projeto contem um estudo de ansible com algumas consirações baseado no repositório contigo em [descomplicando-ansible](./descomplicando-ansible/).

## Instalação

O Ansible é desenvolvido em **Python** logo pode ser instalado com o pip se vc quiser, mas eu particulamente prefiro a instalação do binário diretamente.

Como é um sistema open source vc pode até compilar o código se quiser.

A documentação é tão boa que nem vale a pena reescreve. Mas para resumir, pode instalar usando o gerenciador de pacote da sua distribuição Linux, ou o Brew no Mac e Chocolatey no Windows, só ver no link abaixo.
<https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html>

Versão utilizada durante o estudo foi a 2.10.17

```bash
~/projects/ansible/study-ansible main !1                                                                1.1.7 00:27:24
❯ ansible --version
ansible 2.10.17
  config file = None
  configured module search path = ['/home/david/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  # caminho dos modulos do python
  ansible python module location = /home/david/.asdf/installs/ansible-base/2.10.17/venv/lib/python3.10/site-packages/ansible
  # qual o binario que ele ele vai executar
  executable location = /home/david/.asdf/installs/ansible-base/2.10.17/bin/ansible
  # versao do python
  python version = 3.10.2 (main, Mar 14 2022, 20:47:39) [GCC 11.2.0]
```

O Arquivo de configurações gerais do ansible costuma aparecer em /etc/ansible/ansible.cfg e este vem todo comentado, somente como um exemplo.

A ordem para a procura desse arquivo é:

- 1 nas variaveis de ambiente caso ANSIBLE_CONFIG esteja setado.

- 2 no diretório corrente procurando pelo arquivo ansible.cfg

- 3 no diretório home ~/.ansible.cfg

- 4 em /etc/ansible/ansible.cfg

[Exemplo do arquivo config](./default%20config%20files/ansible.cfg)

[Documentação]<https://github.com/ansible/ansible/blob/stable-2.9/examples/ansible.cfg>

[Exemplo do arquivo hosts](./default%20config%20files/hosts)

Observe que no exemplo ele tem um alinha comentada de inventario que aponta para **/etc/ansible/hosts** que é o arquivo que defini quais máquinas o ansible conhece.

**Vale lembrar que o ansible so precisa ser instalado na máquina de controle, porém as máquinas clientes precisam ter o Python Instalado.**

## Sobre módulos da comunidade

Deve ser sempre vista a pagina do [galaxy](https://galaxy.ansible.com/) e conferir se o módulo precisa de alguma coisa extra.

Por exemplo, o módulo da [aws galaxy](https://galaxy.ansible.com/community/aws) ser instalado com o seguinte comando.

```bash
ansible-galaxy collection install community.aws
```

Além disso se conferir na documentaçao do [aws ansible](https://docs.ansible.com/ansible/latest/collections/amazon/aws/docsite/guide_aws.html#ansible-collections-amazon-aws-docsite-aws-intro).

```bash
pip install boto3
```

Para remover uma collection esta deve ser deletada diretamente removendo a pasta dela. Não existe ainda um parametro para remoção de collection usando o galaxy-ansible.

```bash
rm -rf ~/.ansible/collections/ansible_collections/nomedacollection
```

Existe uma collection interessante que pode ser bem utiliada pelo kubernetes

```bash
ansible-galaxy collection install kubernetes.core
```

## Extras

Para ajudar a melhorar o cõdigo dos playbooks (sequencia de comandos do Ansible em linguagem Yaml) vamos utilizar o [ansible-lint](https://ansible-lint.readthedocs.io/en/latest/).
A instalação é simples mas necessita do python3 instalado. Confira a página do projeto para melhores informações.

```bash
pip3 install ansible-lint
```

## Dica

Sempre que no arquivo hosts vc for definir o interpreter faça nao aponte a para o /usr/bin/python, mas somente para python, que dessa maneira ele paga o interpretador que esta no path.

```yaml
[all:vars]
ansible_python_interpreter=python
```
