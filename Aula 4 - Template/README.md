# Aula 4

Continuando com a partir da aula 3 vamos utilizar o modulo setup que tras o facts do ansible e utilizar os valores coletados dentro do html do index do nginx.

## Template

O Módulo template nada mais faz do que fazer uma interpolação utilizando os facts do ansible para dentro de algum file.

No playbook [playbook 3](../Aula%203%20%20-%20Playbook/my_playbook3.yml) utilizando o modulo copy para copiar o index para dentro do sistema, mas poderiamos trocar o modulo copy para template e fazer interpolação de variaveis em tempo de execução.

Para fazer essa interpolação o ansible utiliza o [jimja](https://jinja.palletsprojects.com/en/3.1.x/) logo o arquivo tem que estar em extensao .j2

Configura o [playbook4](./my_playbook4.yml)

````bash
~/projects/ansible/study-ansible/Aula 4 - Template main !5 ?1                                                                                                                      1.1.7 23:48:09
❯ ansible-playbook my_playbook4.yml

PLAY [Install nginx] ******************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

TASK [Instalando o Nginx] *************************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

TASK [Habilitando service nginx] ******************************************************************************************************************************************************************
ok: [robot-2]
ok: [robot-1]

TASK [Startando service nginx] ********************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

TASK [Copiando o index] ***************************************************************************************************************************************************************************
changed: [robot-1]
changed: [robot-2]

TASK [Copiando o nginx.conf] **********************************************************************************************************************************************************************
ok: [robot-1]
ok: [robot-2]

PLAY RECAP ****************************************************************************************************************************************************************************************
robot-1                    : ok=6    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
robot-2                    : ok=6    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

~/projects/ansible/study-ansible/Aula 4 - Template main !5 ?1                                                                                                                      1.1.7 23:49:16
❯ curl http://robot-1:
<html>
    <head>
        <title>Ansible Study</title>
        <body>
            <h1>DEVOPS GITOPS CLOUDOPS INFRAOPS ops.... ops... ops.. ops.!</h1>
            <h2>O IPS DESSA MAQUINA É: 172.31.8.74</h2> 
        </body>
    </head>
</html>%                        

~/projects/ansible/study-ansible/Aula 4 - Template main !5 ?1                                                                                                                      1.1.7 23:49:27
❯ curl http://robot-2:
<html>
    <head>
        <title>Ansible Study</title>
        <body>
            <h1>DEVOPS GITOPS CLOUDOPS INFRAOPS ops.... ops... ops.. ops.!</h1>
            <h2>O IPS DESSA MAQUINA É: 172.31.9.91</h2> 
        </body>
    </head>
</html>%
````
